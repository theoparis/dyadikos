#pragma once
#include <functional>
#include <utility>
#include <vector>
#include <vulkan/vulkan_raii.hpp>

namespace dyadikos::render {
	struct Device {
		vk::raii::PhysicalDevice physical_device;
		vk::raii::Device device;
	};

	struct Adapter {
		vk::raii::Context context{};
		vk::raii::Instance instance;

		auto request_device(
			std::function<bool(const vk::raii::PhysicalDevice&)> selector
		) -> Device;
	};

	auto request_adapter(
		std::vector<const char*> instance_extensions
	) -> Adapter;

	auto contains_extension(
		std::vector<vk::ExtensionProperties> const& extensionProperties,
		std::string const & extensionName
	) -> bool;
}
