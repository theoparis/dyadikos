#include "three.h"
#include "imgui.h"

int main(int argc, char *argv[]) {
    three::App *app = new three::App(680, 460, "Three Demo");

    app->run([](GLFWwindow *window) {
        ImGui::Begin("Three App");
        ImGui::Text("Hello, world %d", 123);
        ImGui::End();
    });

    return 0;
}
