#include "three.h"
#include "imgui.h"
#include <fstream>
#include <glm/fwd.hpp>
#include <sstream>
#include <string>
#include "glm/glm.hpp"
#include "glm/gtx/transform.hpp"
#include "glm/gtc/type_ptr.hpp"

auto loadFile(std::string path) -> std::string {
    std::ifstream f(path);
    std::stringstream buffer;
    buffer << f.rdbuf();

    return buffer.str();
}

int main(int argc, char *argv[]) {
    three::App *app = new three::App(680, 460, "Three Demo");

    std::string vertexSource = loadFile("resources/example_vert.glsl");
    std::string fragmentSource = loadFile("resources/example_frag.glsl");

    three::Shader *vertexShader =
        new three::Shader(vertexSource, GL_VERTEX_SHADER);
    three::Shader *fragmentShader =
        new three::Shader(fragmentSource, GL_FRAGMENT_SHADER);

    three::ShaderProgram *shaderProgram = new three::ShaderProgram(
        vertexShader->getId(), fragmentShader->getId());

    three::Mesh *mesh = three::primitive::createCube();

    // TODO: camera class
    auto windowSize = app->getWindowSize();
    glm::mat4 cameraTransform =
        glm::perspective(45.0f, windowSize.x / windowSize.y, 0.01f, 1000.0f);
    // Move the camera backwards so it can see the mesh
    cameraTransform *= glm::translate(glm::vec3(0.0, 0.0, -5.0));
    auto modelTransform = three::math::Transform();

    app->run([&shaderProgram, &app, &mesh, &cameraTransform,
              &modelTransform](GLFWwindow *window) {
        ImGui::Begin("Three App");
        ImGui::SliderFloat3("position", glm::value_ptr(modelTransform.position),
                            -10.0f, 10.0f);

        ImGui::End();

        shaderProgram->bind();
        shaderProgram->setUniform("transform",
                                  cameraTransform * modelTransform.getMatrix());
        mesh->draw();
        shaderProgram->unbind();
    });

    return 0;
}
