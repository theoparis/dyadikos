#include <algorithm>
#include <functional>
#include <dyadikos/gpu.hpp>

namespace dyadikos::render {
auto request_adapter(std::vector<const char *> instance_extensions) -> Adapter {
	vk::raii::Context context;
		
	vk::ApplicationInfo application_info{
		"Based",
		1,
		"Dyadikos",
		1,
		VK_API_VERSION_1_1
	};

	vk::InstanceCreateInfo instance_create_info{{}, &application_info};
	instance_create_info.enabledLayerCount = instance_extensions.size();
	instance_create_info.ppEnabledExtensionNames = instance_extensions.data();
	instance_create_info.ppEnabledLayerNames = nullptr;
	instance_create_info.enabledLayerCount = 0;

	vk::raii::Instance instance{context, instance_create_info};

	return {
		.context = std::move(context),
		.instance = std::move(instance),
	};
}

auto Adapter::request_device(
	std::function<bool(const vk::raii::PhysicalDevice&)> selector
) -> dyadikos::render::Device {
	vk::raii::PhysicalDevices physical_devices(instance);
	auto physical_devices_list = std::find_if(
		physical_devices.begin(),
		physical_devices.end(),
		selector
	);
	auto physical_device = physical_devices_list[0];

	std::vector<vk::QueueFamilyProperties> queue_family_properties =
		physical_device.getQueueFamilyProperties();
	auto property_iterator = std::find_if(
		queue_family_properties.begin(),
		queue_family_properties.end(),
		[](vk::QueueFamilyProperties const& qfp) {
			return qfp.queueFlags & vk::QueueFlagBits::eGraphics;
		}
	);
	size_t queue_family_index = std::distance(
		queue_family_properties.begin(),
		property_iterator
	);
	assert(queue_family_index < queue_family_properties.size());

	float queue_priority = 0.0f;
	vk::DeviceQueueCreateInfo device_queue_create_info(
		vk::DeviceQueueCreateFlags(),
		static_cast<uint32_t>(queue_family_index),
		1,
		&queue_priority
	);

	vk::DeviceCreateInfo device_create_info(
		vk::DeviceCreateFlags(),
		device_queue_create_info
	);
	vk::raii::Device device(physical_device, device_create_info);

	return {
		.physical_device = std::move(physical_device),
		.device = std::move(device)
	};
}
auto contains_extension(
	std::vector<vk::ExtensionProperties> const& extensionProperties,
	std::string const & extensionName
) -> bool {
	  auto propertyIterator = std::find_if(
			extensionProperties.begin(),
			extensionProperties.end(),
			[&extensionName]( vk::ExtensionProperties const & ep) {
				return extensionName == ep.extensionName;
			}
		);

	  return (propertyIterator != extensionProperties.end());
	}

}
