#include <Eigen/Eigen>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <vector>
#include <cstddef>
#include <dyadikos/flecs.h>
#include <dyadikos/gpu.hpp>

struct Position {
	Eigen::Vector3f position;
};
 
struct Velocity {
	Eigen::Vector3f velocity;
};

auto main() -> int {
	auto adapter = dyadikos::render::request_adapter({
		"VK_KHR_surface",
		"VK_KHR_headless_surface"
	});

	auto device = adapter.request_device([&](
		[[maybe_unused]]
		const vk::raii::PhysicalDevice& physical_device
	) {
		auto extensions = physical_device.enumerateDeviceExtensionProperties();

		if (
				!dyadikos::render::contains_extension(extensions, "VK_KHR_deferred_host_operations") ||
				!dyadikos::render::contains_extension(extensions, "VK_KHR_acceleration_structure") ||
				!dyadikos::render::contains_extension(extensions, "VK_KHR_swapchain") ||
				!dyadikos::render::contains_extension(extensions, "VK_KHR_pipeline_library") ||
				!dyadikos::render::contains_extension(extensions, "VK_KHR_ray_tracing_pipeline") ||
				!dyadikos::render::contains_extension(extensions, "VK_KHR_ray_query")
		)
				throw std::runtime_error("Missing device extensions");

		return true;
	});
}
