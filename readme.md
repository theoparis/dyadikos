# three.cpp

Three is a C++ game engine that is designed to be simple and easy to use.

## Usage

![Check out the basic example for more information.](examples/basic/src/main.cpp)

## Building From Source

You will need cmake, ninja (or make), a C++17 compiler, GLM, and GLFW installed on your system beforehand.

```bash
cmake -B build -G Ninja
cmake --build build
```

You can then find the compiled examples in build/examples.

### Installation

```bash
cmake --install build
```

