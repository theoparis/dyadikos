#pragma once
#include "mesh.h"

namespace three::primitive {
    auto createCube() -> Mesh *;
    auto createTriangle() -> Mesh *;
} // namespace three::primitive
