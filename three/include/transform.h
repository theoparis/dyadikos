#include "glm/glm.hpp"
#include "glm/gtx/transform.hpp"

namespace three::math {
    struct Transform {
            glm::vec3 position;
            glm::vec3 rotation;
            glm::vec3 scale;

            Transform()
                : position(glm::vec3(0.0, 0.0, 0.0)),
                  rotation(glm::vec3(0.0, 0.0, 0.0)),
                  scale(glm::vec3(1.0, 1.0, 1.0)) {}

            glm::mat4 getMatrix() {
                auto matrix = glm::mat4(1.0f);

                matrix *= glm::translate(position);
                matrix *= glm::rotate(rotation.x, glm::vec3(1, 0, 0));
                matrix *= glm::rotate(rotation.y, glm::vec3(0, 1, 0));
                matrix *= glm::rotate(rotation.z, glm::vec3(0, 0, 1));
                matrix *= glm::scale(scale);

                return matrix;
            }
    };
} // namespace three::math
