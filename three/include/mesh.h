#pragma once
#include <vector>

namespace three {
    class Mesh {
        public:
            Mesh(std::vector<float> vertices);

            void draw();
            void update();

            std::vector<float> vertices;

        private:
            unsigned int vao;
            unsigned int vbo;
    };
} // namespace three
