#include "three.h"
#include "backends/imgui_impl_opengl3.h"
#include "backends/imgui_impl_glfw.h"
#include <stdexcept>

#ifdef __EMSCRIPTEN__
#include "emscripten.h"
#endif

namespace three {
    App::App(int w, int h, std::string programName)
        : windowHeight(h), windowWidth(w), programName(programName) {
        if (!glfwInit()) {
            throw std::runtime_error("Failed to initialize GLFW");
        }

        // setup GLFW window
        glfwWindowHint(GLFW_DOUBLEBUFFER, 1);
        glfwWindowHint(GLFW_DEPTH_BITS, 24);
        glfwWindowHint(GLFW_STENCIL_BITS, 8);

        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

        std::string glsl_version = "";
        glsl_version = "#version 330 core";
#ifdef __EMSCRIPTEN__
        glsl_version = "#version 300 es";
#endif
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        float highDPIscaleFactor = 1.0;
        window = glfwCreateWindow(windowWidth, windowHeight,
                                  programName.c_str(), NULL, NULL);
        if (!window) {
            teardown(NULL);
            throw std::runtime_error("Failed to create GLFW window");
        }
        // watch window resizing
        glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
        glfwMakeContextCurrent(window);
        // VSync
        // glfwSwapInterval(1);

        if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
            teardown(window);
            throw std::runtime_error("Failed to initialize GLAD");
        }

        int actualWindowWidth, actualWindowHeight;
        glfwGetWindowSize(window, &actualWindowWidth, &actualWindowHeight);
        glViewport(0, 0, actualWindowWidth, actualWindowHeight);

        glClearColor(0.2, 0.2, 0.2, 1.0f);
        // Setup Dear ImGui context
        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        ImGuiIO &io = ImGui::GetIO();
        (void)io;
        // io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable
        // Keyboard Controls io.ConfigFlags |=
        // ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

        // Setup Dear ImGui style
        ImGui::StyleColorsDark();
        // ImGui::StyleColorsClassic();

        // Setup Platform/Renderer backends
        ImGui_ImplGlfw_InitForOpenGL(window, true);
        ImGui_ImplOpenGL3_Init(glsl_version.c_str());
    }

    void App::framebuffer_size_callback(GLFWwindow *window, int width,
                                        int height) {
        glViewport(0, 0, width, height);
    }

    void App::teardown(GLFWwindow *window) {
        if (window != NULL) {
            glfwDestroyWindow(window);
        }
        glfwTerminate();
    }

    int App::run(std::function<void(GLFWwindow *)> update) {
        // Render loop
        while (!glfwWindowShouldClose(window)) {
            glfwPollEvents();

            // Start the Dear ImGui frame
            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();

            glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT);

            update(window);

            // Rendering
            ImGui::Render();
            int display_w, display_h;
            glfwGetFramebufferSize(window, &display_w, &display_h);
            glViewport(0, 0, display_w, display_h);
            ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

            glfwSwapBuffers(window);

#ifdef __EMSCRIPTEN__
            emscripten_sleep(1);
#endif
        }

        teardown(window);

        return 0;
    }

    glm::vec2 App::getWindowSize() {
        int width, height;
        glfwGetWindowSize(window, &width, &height);

        glm::vec2 vec = glm::vec2(width, height);

        return vec;
    }
} // namespace three
